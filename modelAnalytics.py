import csv
import math
protocol_list=['tcp','udp','icmp']
service_list=['aol','auth','bgp','courier','csnet_ns','ctf','daytime','discard','domain','domain_u',
             'echo','eco_i','ecr_i','efs','exec','finger','ftp','ftp_data','gopher','harvest','hostnames',
             'http','http_2784','http_443','http_8001','imap4','IRC','iso_tsap','klogin','kshell','ldap',
             'link','login','mtp','name','netbios_dgm','netbios_ns','netbios_ssn','netstat','nnsp','nntp',
             'ntp_u','other','pm_dump','pop_2','pop_3','printer','private','red_i','remote_job','rje','shell',
             'smtp','sql_net','ssh','sunrpc','supdup','systat','telnet','tftp_u','tim_i','time','urh_i','urp_i',
             'uucp','uucp_path','vmnet','whois','X11','Z39_50']
flag_list=['OTH','REJ','RSTO','RSTOS0','RSTR','S0','S1','S2','S3','SF','SH']
tcpmax=[58329.00,1.00,1.00,1.00,1379963888.00
        ,1309937401.00,1.00,3.00,14.00]
linkmax=[101.00,5.00,1.00,7479.00,1.00,
        1.00,7468.00,100.00,5.00,9.00,
        1.00,1.00,1.00]
timemax=[511.00,511.00,1.00,1.00,1.00,
        1.00,1.00,1.00,1.00]
hostmax=[255.00,255.00,1.00,1.00,1.00,
        1.00,1.00,1.00,1.00,1.00]
allmax=tcpmax+linkmax+timemax+hostmax
isBool=[1,2,3,6,11,13,14,20,21]
coe=[0.0]
coe*=257
for i in range(len(coe)):
    coe[i]=100/(i+10)/(i+10)
for i in isBool:
    allmax[i]*=255
for i in range(len(allmax)):
    allmax[i]/=255
def find_index(x,y):
    return [i for i in range(len(y)) if y[i]==x]
def handleProtocol(input):
    if input[1] in protocol_list:
        return find_index(input[1],protocol_list)[0]
def handleService(input):
   if input[2] in service_list:
       return find_index(input[2],service_list)[0]
def handleFlag(input):
    if input[3] in flag_list:
        return find_index(input[3],flag_list)[0]
class AttackType():
    def __init__(self, row):
        self.name=row[257]
        self.row=[]
        for i in range(257):
            row[i]=float(row[i])
        self.row.append(row[0:257])
    def newrow(self,row):
        for i in range(257):
            row[i]=float(row[i])
        self.row.append(row[0:257])
    def score(self,row):
        output=0
        for i in range(41):
            intn=int(row[i])
            decn=row[i]-intn
            if intn>255:
                intn=255
            output+=self.row[i][intn]*(1-decn)
            output+=self.row[i][intn+1]*decn
        return output



        
Attacks=[]
with open('trainedModel.csv',newline='') as csvFile2:
    rows= csv.reader(csvFile2,delimiter=',')
    for row in rows:
        hasAT=False
        for Attack in Attacks:
            if Attack.name==row[257]:
                Attack.newrow(row)
                hasAT=True
        if hasAT==False:
            Attacks.append(AttackType(row))
AttackOutput=[]
with open('kddtrain.csv',newline='') as csvFile2:
    rows= csv.reader(csvFile2,delimiter=',')
    x=0
    y=0
    for row in rows:
        row[1]=handleProtocol(row)   
        row[2]=handleService(row)    
        row[3]=handleFlag(row)
        for i in range(41):
            if row[i] is None:
                row[i] = 0.0
            row[i]=float(row[i])/allmax[i]
        maxScore=0
        maxAttack=""
        for Attack in Attacks:
            score=Attack.score(row)
            if Attack.name=="normal.":
                score*=1.1
            if score>maxScore:
                maxScore=score
                maxAttack=Attack.name
        if len(row)==42:
            if maxAttack!=row[41]:
                y+=1
                print(row[41])
                print(maxAttack)
                print(x)
                print(y)
        x+=1
        
        AttackOutput.append(maxAttack)

def ipLog():
    f = open("data_sort.list", "r")
    dataList = f.readlines()
    result = []
    ip = []
    for d in dataList:
        data = d.split(" ")
        ip.append(data[4]) #ori
        ip.append(data[5]) #dst
        ip.append(data[8]) #port
        ip.append(data[7]) #tcp or udp or icmp
        result.append(ip)
    return result
        
with open('attackDetectionLog.txt', 'w', newline='') as result:
    ipData = ipLog()
    for i in range(len(AttackOutput)):
        attack = AttackOutput[i]
        data = ipData[i]
        resultTxt = attack+" ori: "+data[0]+", dst: "+data[1]+", port: "+data[2]+", type: "+data[3]
        result.write(resultTxt+"\n")
        print(resultTxt)