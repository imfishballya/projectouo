import csv
import math
import copy
protocol_list=['tcp','udp','icmp']
service_list=['aol','auth','bgp','courier','csnet_ns','ctf','daytime','discard','domain','domain_u',
             'echo','eco_i','ecr_i','efs','exec','finger','ftp','ftp_data','gopher','harvest','hostnames',
             'http','http_2784','http_443','http_8001','imap4','IRC','iso_tsap','klogin','kshell','ldap',
             'link','login','mtp','name','netbios_dgm','netbios_ns','netbios_ssn','netstat','nnsp','nntp',
             'ntp_u','other','pm_dump','pop_2','pop_3','printer','private','red_i','remote_job','rje','shell',
             'smtp','sql_net','ssh','sunrpc','supdup','systat','telnet','tftp_u','tim_i','time','urh_i','urp_i',
             'uucp','uucp_path','vmnet','whois','X11','Z39_50']
flag_list=['OTH','REJ','RSTO','RSTOS0','RSTR','S0','S1','S2','S3','SF','SH']
tcpmax=[58329.00,1.00,1.00,1.00,1379963888.00
        ,1309937401.00,1.00,3.00,14.00]
linkmax=[101.00,5.00,1.00,7479.00,1.00,
        1.00,7468.00,100.00,5.00,9.00,
        1.00,1.00,1.00]
timemax=[511.00,511.00,1.00,1.00,1.00,
        1.00,1.00,1.00,1.00]
hostmax=[255.00,255.00,1.00,1.00,1.00,
        1.00,1.00,1.00,1.00,1.00]
allmax=tcpmax+linkmax+timemax+hostmax
isBool=[1,2,3,6,11,13,14,20,21]
coe=[0.0]
coe*=257
for i in range(256):
    coe[i]=100/(i+10)/(i+10)
for i in isBool:
    allmax[i]*=255
for i in range(len(allmax)):
    allmax[i]/=255
class AttackType():
    def __init__(self, name):
        self.name=name
        self.rows=[]
        self.model=[[0.0]]
        self.avg=[0]
        self.avg*=41
        self.totalAvg=0
        self.counter=0
        for i in range(256):
            self.model[0].append(copy.deepcopy(self.model[0][0]))
        for i in range(41):
            self.model.append(copy.deepcopy(self.model[0]))
        self.output=copy.deepcopy(self.model)
    def newrow(self,row):
        self.rows.append(copy.deepcopy(row[:41]))
        self.counter+=1
    def setmodel(self):
        for row in self.rows:
            for i in range(41):
                self.avg[i]+=row[i]
                intn=int(row[i])
                decn=row[i]-intn
                self.model[i][intn]+=1-decn
                self.model[i][intn+1]+=decn
        for i in range(41):
            if i not in isBool:
                self.avg[i]/=self.counter
                self.avg[i]-=AllAvg[i]
                self.avg[i]=abs(self.avg[i])
                self.avg[i]*=self.avg[i]
                self.avg[i]+=0.01
                self.totalAvg+=self.avg[i]
        for i in range(41):
            if i not in isBool:
                self.avg[i]*=32/self.totalAvg
        print(self.name)
        for i in range(41):
            if i ==1:
                for j in range(3):
                    for k in range(3):
                        if j == k:
                            self.output[i][k]+=self.model[i][j]
                        else:
                            self.output[i][k]+=0
            elif i==2:
                for j in range(70):
                    for k in range(70):
                        if j == k:
                            self.output[i][k]+=self.model[i][j]
                        else:
                            self.output[i][k]+=0
            elif i==3:
                for j in range(11):
                    for k in range(11):
                        if j == 9 or k==9:
                            if j == k:
                                self.output[i][k]+=self.model[i][j]
                            else:
                                self.output[i][k]+=0
                        else:
                            if j == k:
                                self.output[i][k]+=1*self.model[i][j]
                            else:
                                self.output[i][k]+=0.1*self.model[i][j]
            elif i in isBool:
                self.output[i][0]=self.model[i][0]
                self.output[i][1]=self.model[i][1]
            else:
                for j in range(256):
                    for k in range(256):
                        self.output[i][k]+=self.model[i][j]*coe[abs(k-j)]
            for j in range(256):
                self.output[i][j]/=len(self.rows)
                if i not in isBool:
                    self.output[i][j]*=self.avg[i]
                else:
                    self.output[i][j]*=1


def gapIs(a,b):
    gap=0
    for i in range(len(a)):
        gap+=(a[i]-b[i])*(a[i]-b[i])
    return math.sqrt(gap)
def find_index(x,y):

    return [i for i in range(len(y)) if y[i]==x]

def handleProtocol(input):
    if input[1] in protocol_list:
        return find_index(input[1],protocol_list)[0]
 
def handleService(input):
   if input[2] in service_list:
       return find_index(input[2],service_list)[0]
 
def handleFlag(input):
    if input[3] in flag_list:
        return find_index(input[3],flag_list)[0]
    

        
Attacks=[]
AllAvg=[0]
AllAvg*=41
lnAT=AttackType("normal.")
Attacks.append(lnAT)
with open('kddtrain.csv',newline='') as csvFile2:
    rows= csv.reader(csvFile2,delimiter=',')
    x=0
    y=0
    z=0
    for row in rows:
        row[1]=handleProtocol(row)   
        row[2]=handleService(row)    
        row[3]=handleFlag(row)
        x+=1
        OuO=False
        for i in range(41):
            row[i]=float(row[i])/allmax[i]
        for Attack in Attacks:
            if Attack.name==row[41]:
                Attack.newrow(row)
                y+=1
                OuO=True
        if OuO==False:
            nAT=AttackType(row[41])
            nAT.newrow(row)
            Attacks.append(nAT)
            z+=1
    print(x)
    print(y)
    print(z)
    
    for i in range(41):
        for j in range(len(Attacks)):
            AllAvg[i]+=Attacks[j].avg[j]
        AllAvg[i]/=len(Attacks)
    for Attack in Attacks:
        Attack.setmodel()
with open('trainedModel.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    for Attack in Attacks:
        for aOutput in Attack.output:
            aOutput+=[Attack.name]
            writer.writerow(aOutput)