import csv
import math
tcpmax=[58329.00,1379963888.00,1309937401.00,1.00,3.00
        ,14.00]
linkmax=[101.00,5.00,1.00,7479.00,1.00,
        1.00,7468.00,100.00,5.00,9.00,
        1.00,1.00,1.00]
timemax=[511.00,511.00,1.00,1.00,1.00,
        1.00,1.00,1.00,1.00]
hostmax=[255.00,255.00,1.00,1.00,1.00,
        1.00,1.00,1.00,1.00,1.00]
allmax=tcpmax+linkmax+timemax+hostmax
class AttackType():
    def __init__(self, row):
        self.name=row[38]
        self.row=[]
        for i in range(38):
            self.row.append(float(row[i]))
    def gap(self,row):
        gap=0.00
        maxgap=0.00
        for i in range(6):
            gap+=(row[i]-self.row[i])*(row[i]-self.row[i])
        if gap>maxgap:
            maxgap=gap
        gap=0
        for i in range(6,19):
            gap+=(row[i]-self.row[i])*(row[i]-self.row[i])
        if gap>maxgap:
            maxgap=gap
        gap=0
        for i in range(19,28):
            gap+=(row[i]-self.row[i])*(row[i]-self.row[i])
        if gap>maxgap:
            maxgap=gap
        gap=0
        for i in range(28,38):
            gap+=(row[i]-self.row[i])*(row[i]-self.row[i])
        if gap>maxgap:
            maxgap=gap
        return maxgap
        
Attacks=[]
with open('trained.csv',newline='') as csvFile2:
    rows= csv.reader(csvFile2,delimiter=',')
    for row in rows:
        Attacks.append(AttackType(row))
AttackOutput=[]
with open('kddtrain.csv',newline='') as csvFile2:
    rows= csv.reader(csvFile2,delimiter=',')
    x=0
    y=0
    for row in rows:
        row.pop(1)
        row.pop(1)
        row.pop(1)
        for i in range(38):
            row[i]=float(row[i])/allmax[i]
        mingap=100
        minAttack=""
        for Attack in Attacks:
            gap=Attack.gap(row)
            if gap<mingap:
                mingap=gap
                minAttack=Attack.name
        if len(row)==39:
            if minAttack!=row[38]:
                y+=1
                print(row[38])
                print(minAttack)
                print(x)
                print(y)
        x+=1
        
        AttackOutput.append(minAttack)
with open('likelyAttack.txt', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    for Attack in AttackOutput:
        writer.writerow([Attack])