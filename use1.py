import csv
import math
tcpmax=[58329.00,1379963888.00,1309937401.00,1.00,3.00
        ,14.00]
linkmax=[101.00,5.00,1.00,7479.00,1.00,
        1.00,7468.00,100.00,5.00,9.00,
        1.00,1.00,1.00]
timemax=[511.00,511.00,1.00,1.00,1.00,
        1.00,1.00,1.00,1.00]
hostmax=[255.00,255.00,1.00,1.00,1.00,
        1.00,1.00,1.00,1.00,1.00]
allmax=tcpmax+linkmax+timemax+hostmax
class AttackType():
    def __init__(self, row):
        self.name=row[38]
        self.row=[]
        for i in range(38):
            row[i]=float(row[i])
        self.row.append(row[0:38])
    def newrow(self,row):
        for i in range(38):
            row[i]=float(row[i])
        self.row.append(row[0:38])
    def gap(self,row):
        mingap=100
        for row2 in self.row:
            gap=0.00
            for i in range(38):
                gap+=(row[i]-row2[i])*(row[i]-row2[i])
            if gap<mingap:
                mingap=gap
        return math.sqrt(mingap)/len(self.row)
        
Attacks=[]
with open('trained1.csv',newline='') as csvFile2:
    rows= csv.reader(csvFile2,delimiter=',')
    for row in rows:
        hasAT=False
        for Attack in Attacks:
            if Attack.name==row[38]:
                Attack.newrow(row)
                hasAT=True
        if hasAT==False:
            Attacks.append(AttackType(row))
AttackOutput=[]
with open('kddtrain.txt',newline='') as csvFile2:
    rows= csv.reader(csvFile2,delimiter=',')
    x=0
    y=0
    for row in rows:
        row.pop(1)
        row.pop(1)
        row.pop(1)
        for i in range(38):
            row[i]=float(row[i])/allmax[i]
        mingap=100
        minAttack=""
        for Attack in Attacks:
            gap=Attack.gap(row)
            if Attack.name=='normal.':
                gap*=0.4
            if gap<mingap:
                mingap=gap
                minAttack=Attack.name
        if len(row)==39:
            if minAttack!=row[38]:
                y+=1
                print(row[38])
                print(minAttack)
                print(x)
                print(y)
        x+=1
        
        AttackOutput.append(minAttack)
with open('likelyAttack.txt', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    for Attack in AttackOutput:
        writer.writerow([Attack])